<?php
class Graphique
{
  private $_nomFichier;

  public function __construct()
  {
    $date = date("dmY");
    $this ->_nomFichier = $date;
  }

  public function Creergraphique()
  {
      // Ajouter des valeurs au graphique
      $file = fopen("CSV/".$this->_nomFichier.".csv","r");
      $resultat = [];
      $moisFr = [];
      $resultat2 =[];
      while (($line = fgetcsv($file)) !== false)
      {
      array_push($resultat,$line[0]);
      array_push($moisFr,$line[2]);
      array_push($resultat2,$line[1]);
      }
      fclose($file);
      $i=24;
      $max = 100;
      $min = 0;
      //Type mime de l'image
      header('Content-type: image/png');
      //Chemin vers le police à utiliser
      $font_file = './arial.ttf';
      //Adapter la largeur de l'image avec le nombre de donnée
      $largeur=$i*50+90;
      $hauteur=400;
      //Hauteur de l'abscisse par rapport au bas de l'image
      $absis=80;
      //Création de l'image
      $courbe=imagecreatetruecolor($largeur, $hauteur);
      //Allouer les couleurs à utiliser
      $bleu=imagecolorallocate($courbe, 0, 0, 255);
      $ligne=imagecolorallocate($courbe, 220, 220, 220);
      $fond=imagecolorallocate($courbe, 250, 250, 250);
      $noir=imagecolorallocate($courbe, 0, 0, 0);
      $rouge=imagecolorallocate($courbe, 255, 0, 0);
      //Colorier le fond
      imagefilledrectangle($courbe,0 , 0, $largeur, $hauteur, $fond);
      //Tracer l'axe des abscisses
      imageline($courbe, 50, $hauteur-$absis, $largeur-10,$hauteur-$absis, $noir);
      //Tracer l'axe des ordonnées
      imageline($courbe, 50,$hauteur-$absis,50,20, $noir);
      //Decaler 10px vers le haut le si le minimum est différent de 0
      if($min!=0)
      {
        $absis+=10;
        $a=10;
      }
      //Nombres des grides verticales
      $nbOrdonne=10;
      //Calcul de l'echelle des abscisses
      $echelleX=($largeur-100)/$i;
      //Calcul de l'echelle des ordonnees
      $echelleY=($hauteur-$absis-20)/$nbOrdonne;
      $i=$min;
      //Calcul des ordonnees des grides
      $py=($max-$min)/$nbOrdonne;
      $pasY=$absis;
        while($pasY<($hauteur-19))
        {
          //Affiche la valeur de l'ordonnee
          imagestring($courbe, 2,10 , $hauteur-$pasY-6, round($i), $noir);
          //Trace la gride
          imageline($courbe, 50, $hauteur-$pasY, $largeur-20,$hauteur-$pasY, $ligne);
          //Decaller vers le haut pour la prochaine gride
          $pasY+=$echelleY;
          //Valeur de l'ordonnee suivante
          $i+=$py;
        }

        $j=-1;
        //Position de la première mois de vente
        $pasX=50;
        //Parcourir le tableau pour le traçage de la diagramme
        foreach ($resultat as $mois => $quantite) {
          //calculer la hateur du point par rapport à sa valeur
          $y=($hauteur) -(($quantite -$min) * ($echelleY/$py))-$absis;
          //dessiner le point
          imagefilledellipse($courbe, $pasX, $y, 6, 6, $rouge);
          //Afficher le mois en français avec une inclinaison de 315°
          imagefttext($courbe, 10, 315, $pasX, $hauteur-$absis+20, $noir, $font_file, $moisFr[$mois]);
          //Tacer une ligne veticale de l'axe de l'abscisse vers le point
          imageline($courbe, $pasX, $hauteur-$absis+$a, $pasX,$y, $noir);
          if($j!==-1)
          {
            //liée le point actuel avec la précédente
            imageline($courbe,($pasX-$echelleX),$yprev,$pasX,$y,$rouge);
          }
          //Afficher la valeur au dessus du point
          imagestring($courbe, 2, $pasX-15,$y-14 , $quantite."C", $rouge);
          $j=$quantite;
     //enregister la hauteur du point actuel pour la liaison avec la suivante
     $yprev=$y;
     //Decaller l'abscisse suivante par rapport à son echelle
     $pasX+=$echelleX;
   }


$j=-1;
//Position de la première mois de vente
$pasX=50;
//Parcourir le tableau pour le traçage de la diagramme
foreach ($resultat2 as $mois => $quantite) {
  //calculer la hateur du point par rapport à sa valeur
  $y=($hauteur) -(($quantite -$min) * ($echelleY/$py))-$absis;
  //dessiner le point
  imagefilledellipse($courbe, $pasX, $y, 6, 6, $bleu);
  //Afficher le mois en français avec une inclinaison de 315°
  imagefttext($courbe, 10, 315, $pasX, $hauteur-$absis+20, $noir, $font_file, $moisFr[$mois]);
  //Tacer une ligne veticale de l'axe de l'abscisse vers le point
  imageline($courbe, $pasX, $hauteur-$absis+$a, $pasX,$y, $noir);
  if($j!==-1)
   {
     //liée le point actuel avec la précédente
     imageline($courbe,($pasX-$echelleX),$yprev,$pasX,$y,$bleu);
   }
   //Afficher la valeur au dessus du point
  imagestring($courbe, 2, $pasX-15,$y-14 , $quantite."%", $bleu);
  $j=$quantite;
  //enregister la hauteur du point actuel pour la liaison avec la suivante
  $yprev=$y;
  //Decaller l'abscisse suivante par rapport à son echelle
  $pasX+=$echelleX;
}
//Envoyer le flux de l'image
$graphique = fopen("GRAPH/".$this->_nomFichier."png","w+");)
imagepng($courbe, $graphique);
//Desallouer le memoire utiliser par l'image
imagedestroy($courbe);
fclose($graphique);
}
}
?>
