<?php

/**
 * Nous créons un classe tableau qui va prendre les données du
 * fichier CSV et les retranscrire en page HTML.
 * Une page HTML est créée par jour et chaque page portera
 * donc la date à laquelle ont été relevé les données (température,
 * humidité et l'heure). Exemple : 01122016.html pour les données récupérées par
 * le NodeMCU le 01 décembre 2016.
 */



class Tableau {
  private $_nomTableau;

  public function __construct() {
    $date = date("dmY");
    $this->_nomTableau = $date;
  }

  public function CreerTableau() {
    $r = "<table>\n\n";
    $r .= "<tr>";
    $r .= "<td>";
    $r .= "Temperature";
    $r .= "</td>";
    $r .= "<td>";
    $r .= "Humidite";
    $r .= "</td>";
    $r .= "<td>";
    $r .= "Heure du releve";
    $r .= "</td>";
    $r .= "</tr>";
    $file = fopen("CSV/".$this->_nomTableau.".csv","r");
    while (($line = fgetcsv($file)) !== false)
    {
            $r .= "<tr>";
            foreach ($line as $cell)
            {
                    $r .= "<td>".$cell."</td>";
            }
            $r .= "</tr>\n";
    }
    fclose($file);
    $r .= "\n</table>";
    $tab = fopen("Tableau/".$this->_nomTableau.".html","w+");
    fputs($tab, $r);
    fclose($tab);

  }

  public function ajoutIndexTableau()
  {
          $indexFichier = fopen("Tableau/indexTableau.txt","a+");
          while($ligne = fgets($indexFichier))
          {
            if($ligne = $this->_nomTableau)
            {
              return;
            }
          }
          fputs($indexFichier, $this->_nomTableau.".html"."\n");
          fclose($indexFichier);

  }




}


?>
